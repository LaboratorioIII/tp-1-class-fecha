package tp1;

public class Fecha {
	private int dia;
	private int mes;
	private int anio;
	private int dia_original;
	private int mes_original;
	private int anio_original;

	private static int[] dias_del_mes;

	/*
	 * Bloque de inicialización estático
	 */
	static {
		dias_del_mes = new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	}

	public Fecha() {
		super();
	}

	public Fecha(int dia, int mes, int anio) {
		super();
		this.dia = dia;
		this.mes = mes;
		this.anio = anio;
		this.dia_original = dia;
		this.mes_original = mes;
		this.anio_original = anio;
	}

	public int getDia() {
		return dia;
	}

	public int getMes() {
		return mes;
	}

	public int getAnio() {
		return anio;
	}

	public void adelantar(int dias, int meses, int años) throws Exception {
		if (dias < 0) {
			throw new Exception("dias debe ser mayor o igual a 0");
		}
		if (meses < 0) {
			throw new Exception("meses debe ser mayor o igual a 0");
		}
		if (años < 0) {
			throw new Exception("años debe ser mayor o igual a 0");
		}

		this.anio += años;
		this.mes += meses;
		this.dia += dias;

		this.normalizar();
	}

	public void reiniciar() {
		this.anio = this.anio_original;
		this.mes = this.mes_original;
		this.dia = this.dia_original;
	}

	public void imprimir() throws Exception {
		throw new Exception(
				"Este método no tiene sentido porque mostrar datos por " + "consola no es responsabilidad de la clasa");
	}

	private Boolean esBisiesto() {
		// Si es divisible por 4 pero no por 100 o si es divisible por 400
		if ((anio % 4 == 0 && anio % 100 != 0) || (anio % 400 == 0)) {
			return true;
		}
		return false;
	}

	private int diasDelMes(int mes) throws Exception {
		if (12 < mes || mes < 1) {
			throw new Exception("El mes debe ser menor o igual a 12 y mayor o igual a 1");
		}
		// Acá resuelvo si es bisiesto.
		if (mes == 2 && this.esBisiesto()) {
			return 29;
		}
		// Acá pido mes-1 porque los arreglos son en base 0
		return Fecha.dias_del_mes[mes - 1];
	}

	private Boolean normalizarMes() {
		// Trato los meses
		// si me paso de 12 meses tengo que resolver cuantos años tengo que
		// sumar
		if (mes > 12) {
			int meses_restantes = mes % 12;
			int años_a_sumar = (int) (mes / 12);
			this.anio += años_a_sumar;
			this.mes = meses_restantes;
			return false;
		}
		return true;
	}

	private Boolean normalizarDia() throws Exception {
		// Trato los dias
		// Guardo el estado inicial
		int mes_inicial = this.mes;
		int dia_inicial = this.dia;
		
		// Me fijo en que mes estoy, resto los dias de este mes, sumo uno a mes
		if (dia_inicial > this.diasDelMes(mes_inicial)) {
			int dias_a_restar = this.diasDelMes(mes_inicial);
			this.dia -= dias_a_restar;
			this.mes++;
			return false;
		}
		return true;
	}

	private void normalizar() throws Exception {

		// ¿¿Pensaron que iban a librar de la recursividad?? :P
		Boolean mes_normalizado = this.normalizarMes();
		Boolean dia_normalizado = this.normalizarDia();
		if (!(mes_normalizado && dia_normalizado)) {
			this.normalizar();
		}
	}

	@Override
	public String toString() {
		return "Fecha [dia=" + dia + ", mes=" + mes + ", anio=" + anio + "]";
	}

}
